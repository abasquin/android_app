package com.example.agnesbsqn.app;

import android.content.Intent;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Home extends AppCompatActivity {

    private EditText home_mdp = null;
    private TextView home_creer_compte = null;
    private CheckBox home_checkbox = null;
    private ImageView home_image = null;
    private Button home_bouton_commencer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        home_creer_compte = findViewById(R.id.home_textview_creeruncompte);
        home_mdp = findViewById(R.id.home_edittext_motdepasse_value);
        home_checkbox = findViewById(R.id.home_checkbox_enregistrermonid);
        home_bouton_commencer = findViewById(R.id.home_button_commencer);
        final EditText home_name = findViewById(R.id.home_editview_name);

        home_creer_compte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Home.this, Choose.class);
                startActivity(intent);
            }
        });

        home_bouton_commencer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                home_mdp.getText().toString();
                if (home_mdp.length() < 8)
                {
                    Toast.makeText(Home.this, getResources().getString(R.string.home_toast_mdptropcourt),
                            Toast.LENGTH_LONG).show();
                }
                else
                {
                    Intent intent = new Intent(Home.this, Choose.class);
                    startActivity(intent);
                    if (home_checkbox.isChecked())
                    {
                        Vibrator vib = (Vibrator) getSystemService(Home.this.VIBRATOR_SERVICE);
                        vib.vibrate(500); //500ms
                    }
                    Toast.makeText(Home.this,getResources().getString(R.string.home_toast_bonjour_xx)+" "+home_name.getText()+" !",
                            Toast.LENGTH_LONG).show();
                }

            }
        });
    }
    public void onClickForgot(View v) {
        Intent intent = new Intent(Home.this, Choose.class);
        startActivity(intent);
    }
}
