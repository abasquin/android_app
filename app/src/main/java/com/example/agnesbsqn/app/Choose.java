package com.example.agnesbsqn.app;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Choose extends AppCompatActivity {

    private EditText choose_item_list_title = null;
    private EditText choose_item_list_subtitle = null;
    private BluetoothAdapter bluetoothAdapter = null;
    private Set<BluetoothDevice> devices = null;
    private Set<BluetoothDevice> bluetoothDevice = null;

    private ArrayList<String[]> listDevice = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);


        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        if (bluetoothAdapter == null)
            Toast.makeText(Choose.this, "Pas de Bluetooth", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(Choose.this, "Ce téléphone est avec Bluetooth", Toast.LENGTH_SHORT).show();

        if (!bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.enable();
        }
        devices = bluetoothAdapter.getBondedDevices();

        listDevice = new ArrayList<String[]>();
        ListView choose_list_view = findViewById(R.id.choose_listview);
        ArrayAdapter adapter = new ArrayAdapter(Choose.this, android.R.layout.simple_list_item_2, android.R.id.text1,listDevice) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                text1.setText(listDevice.get(position)[0]);
                text2.setText(listDevice.get(position)[1]);
                return view; }
        };

        choose_list_view.setAdapter(adapter);
        choose_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(Choose.this, Moove.class);

                intent.putExtra("Device",listDevice.get(i)[0]);
                intent.putExtra("mac_adress", listDevice.get(i)[1]);
                startActivity(intent);
                Toast.makeText(Choose.this,"Vous avez cliqué sur "+listDevice.get(i)[0]+" + "+listDevice.get(i)[1],
                        Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    protected void onResume(){
        super. onResume ();

        listDevice.clear();
        for (BluetoothDevice blueDevice : devices) {
            listDevice.add(new String[]{blueDevice.getName(),blueDevice.getAddress()});
        }
    }

}

