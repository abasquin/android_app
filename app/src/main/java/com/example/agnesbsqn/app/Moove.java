package com.example.agnesbsqn.app;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

public class Moove extends AppCompatActivity {

    private ImageButton moove_button_up = null;
    private ImageButton moove_button_right = null;
    private ImageButton moove_button_left = null;
    private ImageButton moove_button_down = null;
    private TextView moove_temperature_value = null;
    private TextView moove_humidite_value = null;
    private String moove_previous_device_id = null;
    private String moove_previous_device_macadress = null;
    private BluetoothAdapter bluetoothAdapter = null;
    private BluetoothDevice bluetoothDevice = null;

    private String temp = new String(),hum = new String();
    private BluetoothGatt gatt = null;
    private List<BluetoothGattService> listGattServices = null;
    private List<BluetoothGattCharacteristic> listGattCharacteristics = null;
    public ProgressDialog progressDialog = null;

    private void setText(final TextView text,final String value){
        runOnUiThread(new Runnable() {
        @Override
        public void run() {
            text.setText(value);
        }
        });
    }

    private void commandToRobot(int index, boolean go){
        String data = "!B" + String.valueOf(index) + (go ? "1" : "0");
        ByteBuffer buffer = ByteBuffer.allocate(data.length()).order(java.nio.ByteOrder.LITTLE_ENDIAN);
        buffer.put(data.getBytes());
        BluetoothGattCharacteristic charac = listGattServices.get(4).getCharacteristics().get(1);
        charac.setValue(sendDataWithCRC(buffer.array()));
        boolean status = gatt.writeCharacteristic(charac);
    }

    protected byte[] sendDataWithCRC(byte[] data) { // Calculate checksum
        byte checksum = 0;
        for (byte aData : data) {
            checksum += aData;
        }
        checksum = (byte) (~checksum);// Invert
        // Add crc to data
        byte dataCrc[] = new byte[data.length + 1]; System.arraycopy(data, 0, dataCrc, 0, data.length); dataCrc[data.length] = checksum;
        return dataCrc;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moove);

        moove_button_up = findViewById(R.id.moove_imagebutton_flechehaut);
        moove_button_right = findViewById(R.id.moove_imagebutton_flechedroit);
        moove_button_left = findViewById(R.id.moove_imagebutton_flechegauche);
        moove_button_down = findViewById(R.id.moove_imagebutton_flechebas);
        moove_temperature_value = findViewById(R.id.moove_textview_temperature_value);
        moove_humidite_value = findViewById(R.id.moove_textview_humidite_value);

        Intent previousIntent_deviceid = getIntent();
        Intent previousIntent_macadress = getIntent();


        //Progress dialogue
        progressDialog = new ProgressDialog(Moove.this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Preparing to connect ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show() ;


        if (previousIntent_deviceid.hasExtra("Device")) {
            moove_previous_device_id = previousIntent_deviceid.getStringExtra("Device");
        } else {
            finish();
        }
        if (previousIntent_macadress.hasExtra("mac_adress")) {
            moove_previous_device_macadress = previousIntent_macadress.getStringExtra("mac_adress");
        } else {
            finish();
        }

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothDevice = bluetoothAdapter.getRemoteDevice(moove_previous_device_macadress);

        BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                if (newState == BluetoothAdapter.STATE_CONNECTED){
                    gatt.discoverServices();
                    progressDialog.dismiss();
                    progressDialog.cancel();
                }
            }
            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                listGattServices = gatt.getServices();
                listGattCharacteristics = listGattServices.get(4).getCharacteristics();
                boolean res = gatt.readCharacteristic(listGattCharacteristics.get(0));
                boolean res_bis = gatt.setCharacteristicNotification(listGattCharacteristics.get(0), true);
                BluetoothGattDescriptor desc = listGattCharacteristics.get(0).getDescriptors().get(1);
                desc.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                if (!gatt.writeDescriptor(desc)) {
                    Log.i("MY_ERROR", "problem");
                    return;
                }
            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                super.onCharacteristicChanged(gatt, characteristic);
                if (listGattCharacteristics != null) {
                    byte[] u = characteristic.getValue();
                    if (u != null) {
                        String readMessage = new String(u);
                        String data_received = new String(u);
                        data_received += readMessage;
                        int pos_temp = data_received.indexOf("Temp:");
                        if (pos_temp + 11 <= data_received.length() && pos_temp != -1) {
                            temp = data_received.substring(pos_temp + 6, pos_temp + 11);
                            data_received = data_received.subSequence(pos_temp + 11, data_received.length()).toString();
                        }

                        int pos_hum = data_received.indexOf("Hum:");
                        if (pos_hum + 10 <= data_received.length() && pos_hum != -1) {
                            hum = data_received.substring(pos_hum + 5, pos_hum + 10);
                            data_received = data_received.subSequence(pos_hum + 10, data_received.length()).toString();
                        }
                        setText(moove_temperature_value,temp);
                        setText(moove_humidite_value,hum);
                    }
                }
            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                super.onCharacteristicRead(gatt, characteristic, status);
                try {
                    if(listGattCharacteristics!=null) {
                        byte[] u = characteristic.getValue();
                        if (u!=null) {
                            String readMessage = new String(u);
                            Log.i("TEST", readMessage);
                        }
                    }
                }finally{};
            }
        };
        gatt = bluetoothDevice.connectGatt(Moove.this, true, gattCallback);


        moove_button_up.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                if (arg1.getAction()==MotionEvent.ACTION_DOWN){
                        Log.d("bouton haut : ", "up 1");
                        commandToRobot(5, true);
                    return true;}
                else if (arg1.getAction() == MotionEvent.ACTION_UP){
                        Log.d("bouton haut : ", "up 0");
                        commandToRobot(5, false);
                    return true;}
                return false;
            }
        });
        moove_button_right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                if (arg1.getAction()==MotionEvent.ACTION_DOWN){
                    Log.d("bouton droit : ", "right 1");
                    commandToRobot(7, true);

                    return true;}
                else if (arg1.getAction() == MotionEvent.ACTION_UP){
                    Log.d("bouton droit : ", "right 0");
                    commandToRobot(7, false);
                    return true;}
                return false;
            }
        });
        moove_button_left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                if (arg1.getAction()==MotionEvent.ACTION_DOWN){
                    Log.d("bouton gauche : ", "left 1");
                    commandToRobot(8, true);
                    return true;}
                else if (arg1.getAction() == MotionEvent.ACTION_UP){
                    Log.d("bouton gauche : ", "left 0");
                    commandToRobot(8, false);
                    return true;}
                return false;
            }
        });
        moove_button_down.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                if (arg1.getAction()==MotionEvent.ACTION_DOWN){
                    Log.d("bouton bas : ", "down 1");
                    commandToRobot(6, true);

                    return true;}
                else if (arg1.getAction() == MotionEvent.ACTION_UP){
                    Log.d("bouton bas : ", "down 0");
                    commandToRobot(5, false);
                    return true;}
                return false;
            }
        });
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        gatt.close();
    }
}
